# HerkunftRaten

Eine Webapp um auf der geografischen Karte Deutschlands Distanz-Schaetz-Spiele spielen zu koennen.

## Anleitung

Die zum Aufloesen genutzten Punkte werden in der Variable `solutions` gespeichert..
Man kann sich die Werte von ausgesuchten Positionen ausgeben lassen, indem man auf der Karte alle Punkte anklickt (es werden keine Punkte gezeichnet, aber die Werte werden trotzdem gespeichert!), danach auf den "Save clicks" Button am Ende der Seite drueckt und die ausgegebenen Werte der Variable `solutions` zuweist. Die Buttons fuer die Loesungen werden automatisch generiert. 

## TODO

#### Code

- [ ] clean up code!

#### ~~logic~~

- [x] ~~differ between different players (change color)~~
- [x] ~~set player number~~
- [x] ~~calculate distance to goal and print winner~~

- [x] ~~read goals from text file~~
- [x] ~~create buttons dynamically~~
