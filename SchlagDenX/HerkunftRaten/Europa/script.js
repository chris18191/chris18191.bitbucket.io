// CONFIGURATION
var imagePath = "map.png";
var pointSize = 15;
var fontsize = "10em";
var fontfamily = "Arial";
var lineWidth = 5;
var clickCounter = 0;
var solutionSteps = 50;
var playerClicks = [];
var allClicks = [];
var players = [{color:"darkgoldenrod", name:"Solution"},
               {color:"darkred", name:"Spieler 1"},
               {color:"darkblue", name:"Spieler 2"}];
solutions=[{"x":568.3391003460208,"y":983.6887381111978},{"x":791.522491349481,"y":790.0287981385127}, {"x":1042.3875432525952,"y":1255.158475394337}, {"x":99.48096885813149,"y":252.2766433929313},{"x":817.4740484429066,"y":912.7953672283398}, {"x":578.719723183391,"y":1334.69737931169},{"x":974.9134948096886,"y":988.8760579318948}, {"x":147.92387543252596,"y":1533.544639105072}, {"x":642.7335640138408,"y":959.4812456146122}];
// END OF CONFIGURATION

var t=0;

var canvas = document.getElementById("mapCanvas");
var saveButton = document.getElementById("saveButton");
var ctx = canvas.getContext("2d");
var img = document.getElementById("map");

canvas.addEventListener("mouseup", mouseClick, false);
saveButton.addEventListener("click", function() {
  confirm(JSON.stringify(allClicks));
});

window.onload = function() {
  var div = document.getElementById("buttonbar");
  for(var i=0; i<solutions.length; i++){
    var element = document.createElement("button");
    element.value = i;
    element.innerHTML = "Ort "+(i+1);
    element.addEventListener("click", function(){
      console.log(solutions[this.value]);
      revealSolution(solutions[this.value]);
    });
    
    div.appendChild(element);
  }
}

var imageObj = new Image();
img.src = imagePath;
img.onload = function(){
  var W = img.width;
  var H = img.height;
  canvas.width = W;
  canvas.height = H;
  ctx.drawImage(img, 0, 0); //draw image

  //resize
  resample_single(canvas, canvas.width, canvas.height, true);


}

function clearCanvas(){
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(img, 0, 0);
}

document.addEventListener('keydown', function(event) {
  switch(event.keyCode) {
    //Space
    case 32:
      break;
    //Backspace
    case 8:
      clearCanvas();
      playerClicks = []
      clickCounter = 0;
      break;
    
    //Enter
    case 13:
      break;
      drawCircle(1000, 1000, pointSize, players[0].color);
      confirm();
      revealSolution({x:1000 , y:1000});
      break;
  }
});

/**
 * Hermite resize - fast image resize/resample using Hermite filter. 1 cpu version!
 *
 * @param {HtmlElement} canvas
 * @param {int} width
 * @param {int} height
 * @param {boolean} resize_canvas if true, canvas will be resized. Optional.
 */
function resample_single(canvas, width, height, resize_canvas) {
  var width_source = canvas.width;
  var height_source = canvas.height;
  width = Math.round(width);
  height = Math.round(height);

  var ratio_w = width_source / width;
  var ratio_h = height_source / height;
  var ratio_w_half = Math.ceil(ratio_w / 2);
  var ratio_h_half = Math.ceil(ratio_h / 2);

  var ctx = canvas.getContext("2d");
  var img = ctx.getImageData(0, 0, width_source, height_source);
  var img2 = ctx.createImageData(width, height);
  var data = img.data;
  var data2 = img2.data;

  for (var j = 0; j < height; j++) {
    for (var i = 0; i < width; i++) {
      var x2 = (i + j * width) * 4;
      var weight = 0;
      var weights = 0;
      var weights_alpha = 0;
      var gx_r = 0;
      var gx_g = 0;
      var gx_b = 0;
      var gx_a = 0;
      var center_y = (j + 0.5) * ratio_h;
      var yy_start = Math.floor(j * ratio_h);
      var yy_stop = Math.ceil((j + 1) * ratio_h);
      for (var yy = yy_start; yy < yy_stop; yy++) {
        var dy = Math.abs(center_y - (yy + 0.5)) / ratio_h_half;
        var center_x = (i + 0.5) * ratio_w;
        var w0 = dy * dy; //pre-calc part of w
        var xx_start = Math.floor(i * ratio_w);
        var xx_stop = Math.ceil((i + 1) * ratio_w);
        for (var xx = xx_start; xx < xx_stop; xx++) {
          var dx = Math.abs(center_x - (xx + 0.5)) / ratio_w_half;
          var w = Math.sqrt(w0 + dx * dx);
          if (w >= 1) {
            //pixel too far
            continue;
          }
          //hermite filter
          weight = 2 * w * w * w - 3 * w * w + 1;
          var pos_x = 4 * (xx + yy * width_source);
          //alpha
          gx_a += weight * data[pos_x + 3];
          weights_alpha += weight;
          //colors
          if (data[pos_x + 3] < 255)
            weight = weight * data[pos_x + 3] / 250;
          gx_r += weight * data[pos_x];
          gx_g += weight * data[pos_x + 1];
          gx_b += weight * data[pos_x + 2];
          weights += weight;
        }
      }
      data2[x2] = gx_r / weights;
      data2[x2 + 1] = gx_g / weights;
      data2[x2 + 2] = gx_b / weights;
      data2[x2 + 3] = gx_a / weights_alpha;
    }
  }
  //clear and resize canvas
  if (resize_canvas === true) {
    canvas.width = width;
    canvas.height = height;
  } else {
    ctx.clearRect(0, 0, width_source, height_source);
  }
  img = img2;
  //draw
  ctx.putImageData(img, 0, 0);
}

function  getMousePos(evt) {
  var rect = canvas.getBoundingClientRect(), // abs. size of element
      scaleX = canvas.width / rect.width,    // relationship bitmap vs. element for X
      scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y

  var x = (evt.clientX - rect.left) * scaleX;   // scale mouse coordinates after they have
  var y = (evt.clientY - rect.top) * scaleY;    // been adjusted to be relative to element

  return {x:x, y:y};
}

function drawCircle( centerX, centerY, radius, color){
  ctx.beginPath();
  ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
  ctx.fillStyle = color;
  ctx.fill();
  ctx.closePath();
}

function mouseClick(evt){
  var pos = getMousePos(evt);
  clickCounter += 1;
  console.log("["+clickCounter+"] x: "+pos.x+" y:"+pos.y);
  allClicks.push({x: pos.x, y: pos.y});
  if(clickCounter < players.length){
    playerClicks.push({x: pos.x, y: pos.y});
  }
}

function revealSolution(solPos){
  
  
  var p1 = playerClicks[0];
  var p2 = playerClicks[1];
  
  drawCircle(p1.x, p1.y, pointSize, players[1].color);
  drawCircle(p2.x, p2.y, pointSize, players[2].color);
  
  var dist1 = Math.sqrt( Math.pow((p1.x-solPos.x), 2) + Math.pow((p1.y-solPos.y), 2) );
  var dist2 = Math.sqrt( Math.pow((p2.x-solPos.x), 2) + Math.pow((p2.y-solPos.y), 2) );
  var dist = [dist1, dist2];
  
  if(dist1<dist2){
    console.log("Player1 wins");
    showWinner(players[1]);
    winner = 1;
    loser = 2;
  }else if(dist2<dist1){
    console.log("Player2 wins");
    showWinner(players[2]);
    winner = 2;
    loser = 1;
  }else{
    alert("Unentschieden!");
  }
  
  var angel1 = getAngel(solPos, p1),
      angel2 = getAngel(solPos, p2);
      
  var stepLength = dist[winner-1]/solutionSteps;
  
  animateLineDrawing(stepLength, [angel1, angel2], solPos, loser);
  
}

function getAngel(p1, p2){
  var dx = p2.x - p1.x;
  var dy = p2.y - p1.y;
  
  // dx and dy positive, 1st quadrant
  var constant = 90;
  
  if(dx>0){
    if(dy>0){
      // dx pos, dy neg, 2nd quadrant
      constant = 90;
    }
  }else{
    if(dy>0){
      // dx and dy negative, 3rd quadrant
      constant = 270;
    } else {
      // dx neg, dy pos, 4th quadrant
      constant = 270;
    }
  }
  var gradient = dy/dx;
  return Math.atan(gradient)*180/Math.PI + constant; // converts angel from radiants to degrees
}

var latestLoserPos = null;

function animateLineDrawing(stepLength, angels, solPos, loser){
  
  if (t <= solutionSteps) {
      requestAnimationFrame(function (){
        animateLineDrawing(stepLength, angels, solPos, loser);
      });
  } else {
    var dist = Math.sqrt(Math.pow(playerClicks[loser-1].x - latestLoserPos.x, 2)+
               Math.pow(playerClicks[loser-1].y - latestLoserPos.y, 2));
    drawLine(latestLoserPos.x, latestLoserPos.y, angels[loser-1], dist, "#0d0");
    t = 0;
    latestLoserPos = null;
    drawCircle(solPos.x, solPos.y, pointSize, players[0].color);
  }
  // draw a line segment from the last waypoint
  // to the current waypoint
  for(var i=1;i<players.length;i++){
    var endpoint = drawLine(solPos.x, solPos.y, angels[i-1], stepLength*t, players[i].color);
    if(i == loser){
      latestLoserPos = endpoint;
    }
  }
  // increment "t" to draw further
  t++
}

function showWinner(player){
  ctx.fillStyle = player.color;
  ctx.fillRect(0,0, canvas.width, canvas.height/10);
  ctx.fillStyle = "white";
  ctx.font = fontsize+" "+fontfamily;
  ctx.textAlign = "center";
  ctx.fillText("Punkt an "+player.name+"!", canvas.width/2, canvas.height/15);
}

function drawLine(x, y, angel, length, color="#000"){
  theta = Math.PI*2*(angel-90)/360;
  ctx.beginPath()
  ctx.moveTo(x, y);
  var newX = x + length * Math.cos(theta),
      newY = y + length * Math.sin(theta);
  ctx.lineTo(newX, newY);
  ctx.lineWidth = lineWidth;
  ctx.strokeStyle = color;
  ctx.stroke();
  ctx.strokeStyle = "#000";
  ctx.closePath();
  return {x: newX, y: newY};
}

function testDrawLine(angels){
  for(var i=0;i<angels.length;i++){
    var angel = angels[i];
    result = drawLine(1000,1000, angel, 300);
    ctx.font = fontsize+" "+fontfamily;
    ctx.fillText(angels[i]+"Grad", result.x, result.y);
  }
}
